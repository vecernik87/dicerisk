<?php

namespace App;

use Nette,
	 Nette\Application\Routers\RouteList,
	 Nette\Application\Routers\Route,
	 Nette\Application\Routers\SimpleRouter;

/**
 * Router factory.
 */
class RouterFactory {

	/**
	 * @return \Nette\Application\IRouter
	 */
	public function createRouter() {
		$router = new RouteList();
		$router[] = new Route('game/g<game_id [0-9]+>', 'Game:default');
		$router[] = new Route('<presenter>/[<action [a-z\-]+>/][<game_id [0-9]+>]', 'Homepage:default');
		return $router;
	}

}
