<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;


if ($_SERVER['SERVER_ADDR']== '192.168.87.61' || in_array($_SERVER['REMOTE_ADDR'], array('84.42.149.108'))) {
	$configurator->setDebugMode(TRUE);  // debug mode MUST NOT be enabled on production server
}
$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
		  ->addDirectory(__DIR__)
		  ->addDirectory(__DIR__ . '/../vendor/others')
		  ->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.db.neon');

$container = $configurator->createContainer();

return $container;
