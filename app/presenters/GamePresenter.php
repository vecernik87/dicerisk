<?php

namespace App\Presenters;

use Nette,
	 App\Model,
	 \Nette\Forms\Form;

class GamePresenter extends BasePresenter {

	/**
	 * @var \Model\GameManager
	 * @inject
	 */
	public $gameManager;
	private $gameStatusText = array(
		 \Game::STATUS_PREPARE => 'příprava hry',
		 \Game::STATUS_PLAY => 'hra probíhá',
		 \Game::STATUS_END => 'konec hry',
	);

	public function getGameStatusText($status) {
		if (array_key_exists($status, $this->gameStatusText)) {
			return $this->gameStatusText[$status];
		} else {
			return 'neznámý';
		}
	}

	protected function createComponentGamenewForm() {
		$form = new Nette\Application\UI\Form;
		$form->addText('name', 'Název hry')
				  ->setRequired('Prosím zadejte název hry.');
		$form->addSelect('map_id', 'Mapa', $this->gameManager->getMapList4Select());
		$form->addText('size', 'Počet hráčů')
				  ->setType('number')
				  ->addRule(Form::INTEGER, 'Počet hráčů musí být číslo')
				  ->addRule(Form::RANGE, 'Počet hráčů musí být od 2 do 8', array(2, 8));
		$form->addSubmit('send', 'Založit');
		$form->onSuccess[] = $this->gamenewFormSucceeded;
		return $this->formatFormHorizontal($form);
	}

	public function gamenewFormSucceeded($form) {
		$this->forbidAnonymous();
		$values = $form->getValues();
		$newgame = $this->gameManager->createGame($this->user->id, $values->map_id, $values->size, $values->name);
		if ($newgame) {
			$this->flashMessage('Založení hry #' . $newgame->getData()->id . ' bylo úspěšné.', 'success');
			$this->redirect('joinGame!', array('game_id' => $newgame->getData()->id, 'redir' => 1));
		} else {
			$this->flashMessage('Nepodařilo se založit hru :(', 'danger');
		}
	}

	public function renderNew() {
		$this->forbidAnonymous();
	}

	public function renderListAll() {
		$this->template->setFile('../app/templates/Game/list.latte');
		$this->template->gameList = $this->gameManager->getGameList();
	}

	public function renderListMy() {
		$this->forbidAnonymous();
		$this->template->setFile('../app/templates/Game/list.latte');
		$this->template->gameList = $this->gameManager->getGameList($this->user->id);
	}

	public function handleJoinGame($game_id, $redir) {
		$this->forbidAnonymous();
		if ($this->gameManager->getGame($game_id)->joinUser($this->user->id)) {
			$this->flashMessage('Úspěšně připojen ke hře.', 'success');
		} else {
			$this->flashMessage('Nepodařilo se připojit ke hře. :(', 'danger');
		}
		if ($redir == 1) {
			$this->redirect('Game:', array('game_id' => $game_id));
		}
		$this->redirect('this');
	}

	public function handleLeaveGame($game_id) {
		$this->forbidAnonymous();
		if ($this->gameManager->getGame($game_id)->leaveUser($this->user->id)) {
			$this->flashMessage('Úspěšně odpojen od hry.', 'success');
		} else {
			$this->flashMessage('Nepodařilo se odpojit. :(', 'danger');
		}
		$this->redirect('this');
	}

	public function handleStartGame($game_id) {
		$this->forbidAnonymous();
		if ($this->gameManager->getGame($game_id)->start($this->user->id)) {
			$this->flashMessage('Hra spuštěna!', 'success');
		} else {
			$this->flashMessage('Hru se nepodařilo spustit', 'danger');
		}
		$this->redirect('this');
	}

	public function handleStepGame($game_id) {
		$this->forbidAnonymous();
		
		if(!$this->gameManager->getGame($game_id)->setStep($this->user->id, date("Y-m-d H:i:s") . ' random data')){
			$this->flashMessage('Nepodařilo se provést tah.','danger');
		}
		$this->redirect('this');
	}

	public function renderDefault($game_id) {
		if ($game_id === NULL) { // if no game specified, redirect to list
			$this->redirect('Game:listAll');
		}

		try {
			$game = $this->gameManager->getGame($game_id);
		} catch (\GameNotFoundException $e) {
			$this->flashMessage('Požadovaná hra nebyla nalezena.', 'warning');
			$this->redirect('Game:listAll');
		}
		$this->template->gObj = $game; // everything went better than expected :) lets send object and data to template
		$this->template->gRow = $game->getData();
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////
	public function renderTest() {
		$game = $this->gameManager->getGame(2);
		$uid = null;
		for ($i = 0; $i < 5; $i++) {
			$uid = $game->getNextPlayer($uid);
			dump($uid);
		}
		//$this->gameManager->getGame(1)->setStep(1, 'asdf');
	}

	public function renderMap() { // move this shit out of there!
		$map_id = 1;
		$map = $this->gameManager->getMap($map_id);
		$this->template->regions = $map['regions'];
		$this->template->size = $map['size'];
	}

}
