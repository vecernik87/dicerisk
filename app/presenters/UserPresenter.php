<?php

namespace App\Presenters;

use Nette,
	 App\Model;

/**
 * Sign in/out presenters.
 */
class UserPresenter extends BasePresenter {
    /** @persistent */
    public $backlink = '';
	/**
	 * @var \App\UserManager
	 * @inject
	 */
	public $userManager;

	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentLoginForm() {
		$form = new Nette\Application\UI\Form;
		$form->addText('name')
				  ->setAttribute('placeholder', 'Jméno')
				  ->setRequired('Prosím zadejte jméno.');
		$form->addPassword('pass')
				  ->setAttribute('placeholder', 'Heslo')
				  ->setRequired('Prosím zadejte heslo.');
		//$form->addCheckbox('remember', 'Keep me signed in');
		$form->addSubmit('send', 'Přihlásit');
		$form->onSuccess[] = $this->loginFormSucceeded;
		return $this->formatFormHorizontal($form);
	}

	/**
	 * register form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentRegisterForm() {
		$form = new Nette\Application\UI\Form;
		$form->addText('name')
				  ->setAttribute('placeholder', 'Jméno')
				  ->setRequired('Prosím zadejte jméno.');
		$form->addPassword('pass')
				  ->setAttribute('placeholder', 'Heslo')
				  ->setRequired('Prosím zadejte heslo.');
		$form->addSubmit('send', 'Registrovat');

		$form->onSuccess[] = $this->registerFormSucceeded;
		return $this->formatFormHorizontal($form);
	}

	public function loginFormSucceeded($form) {
		$values = $form->getValues();

		/* if ($values->remember) {
		  $this->getUser()->setExpiration('14 days', FALSE);
		  } else {
		  $this->getUser()->setExpiration('20 minutes', TRUE);
		  } */
		$this->getUser()->setExpiration('2 days', TRUE);
		try {
			$this->getUser()->login($values->name, $values->pass);
			$this->restoreRequest($this->backlink);
			$this->redirect('Homepage:');
		} catch (Nette\Security\AuthenticationException $e) {
			$this->flashMessage('Nepodařilo se přihlásit, zadejte prosím správné údaje.', 'danger');
			//$form->addError($e->getMessage());
		}
	}

	public function registerFormSucceeded($form) {
		$values = $form->getValues();
		$success = $this->userManager->add($values->name, $values->pass);
		if ($success) {
			$this->flashMessage('Registrace byla úspěšná.', 'success');
			$this->getUser()->login($values->name, $values->pass);
			$this->redirect('Homepage:');
		} else {
			$this->flashMessage('Zvolte jiné uživatelské jméno.', 'danger');
		}
	}

	public function actionLogout() {
		$this->getUser()->logout(true);
		$this->flashMessage('Odhlášení bylo úspěšné.', 'success');
		$this->redirect('User:login');
	}

	public function renderLogin() {
		if ($this->user->isLoggedIn()) {
			$this->flashMessage('Ne, tohle vážně nejde.', 'warning');
			$this->redirect('Homepage:');
		}
	}

	public function renderRegister() {
		if ($this->user->isLoggedIn()) {
			$this->flashMessage('Ne, tohle vážně nejde.', 'warning');
			$this->redirect('Homepage:');
		}
	}

}
