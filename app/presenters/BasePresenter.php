<?php

namespace App\Presenters;

use Nette,
	 App\Model,
	 Nette\Forms\Form,
	 Nette\Forms\Controls;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

	protected function formatFormHorizontal(Nette\Forms\Form $form) {
		$renderer = $form->getRenderer();
		$renderer->wrappers['controls']['container'] = NULL;
		$renderer->wrappers['pair']['container'] = 'div class=form-group';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['control']['container'] = 'div class="col-sm-12 text-center"';
		//$renderer->wrappers['label']['container'] = 'div class="col-sm-3 control-label"';
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
		$form->getElementPrototype()->class('form-horizontal');
		foreach ($form->getControls() as $control) {

			if ($control instanceof Controls\Button) {
				$control->setAttribute('class', empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-default');
				$usedPrimary = TRUE;
			} elseif ($control instanceof Controls\TextBase || $control instanceof Controls\SelectBox || $control instanceof Controls\MultiSelectBox) {
				$control->setAttribute('class', 'form-control');
			} elseif ($control instanceof Controls\Checkbox || $control instanceof Controls\CheckboxList || $control instanceof Controls\RadioList) {
				$control->getSeparatorPrototype()->setName('div')->class($control->getControlPrototype()->type);
			}

			if ($control->isRequired()) {
				foreach ($control->getRules()->getIterator() as $rule) {
					if ($rule->operation == ':filled') {
						//dump($rule->message);
						// TODO - tohle by se melo udelat v netteForms.js, ale tam se neposila element jako element nybrz jako node, takze to nastavit nejde...
						$control->setAttribute('onkeyup', 'if(this.value==\'\'){this.setCustomValidity(\'' . $rule->message . '\')}else{this.setCustomValidity(\'\')}');
					}
				}
			}
		}
		return $form;
	}
	
	protected function forbidAnonymous(){
		if ($this->user->isLoggedIn()){
			return;
		}else{
			$this->flashMessage('Chyba digestoře! Oholte prosím kivi, přihlašte se a zkuste to znovu.','danger');
			$this->redirect('User:login',array('backlink' => $this->storeRequest()));
		}
	}

}
