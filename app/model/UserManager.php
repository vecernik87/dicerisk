<?php

namespace App;

use Nette,
	 Nette\Utils\Strings;

/**
 * Users management.
 */
class UserManager extends Nette\Object implements Nette\Security\IAuthenticator {

	const
			  TABLE_NAME = 'user',
			  COLUMN_ID = 'id',
			  COLUMN_NAME = 'name',
			  COLUMN_PASSWORD_HASH = 'pass',
			  COLUMN_ROLE = 'role';

	/** @var Nette\Database\Context */
	private $database;

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}

	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials) {
		list($username, $password) = $credentials;
		$password = self::removeCapsLock($password);

		$row = $this->database->table(self::TABLE_NAME)->where(self::COLUMN_NAME, $username)->fetch();

		if (!$row || !Passwords::verify($password, $row[self::COLUMN_PASSWORD_HASH])) {
			throw new Nette\Security\AuthenticationException('Wrong username or password', self::INVALID_CREDENTIAL);
		} elseif (Passwords::needsRehash($row[self::COLUMN_PASSWORD_HASH])) {
			$row->update(array(
				 self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
			));
		}

		$arr = $row->toArray();
		unset($arr[self::COLUMN_PASSWORD_HASH]);
		return new Nette\Security\Identity($row[self::COLUMN_ID], $row[self::COLUMN_ROLE], $arr);
	}

	/**
	 * Adds new user.
	 * @param  string
	 * @param  string
	 * @return void
	 */
	public function add($username, $password) {
		$row = $this->database->table(self::TABLE_NAME)->where('name = ?', $username)->fetch();
		if (!$row) {
			$this->database->table(self::TABLE_NAME)->insert(array(
				 self::COLUMN_NAME => $username,
				 self::COLUMN_PASSWORD_HASH => Passwords::hash(self::removeCapsLock($password)),
			));
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Fixes caps lock accidentally turned on.
	 * @return string
	 */
	private static function removeCapsLock($password) {
		return $password === Strings::upper($password) ? Strings::lower($password) : $password;
	}

}
