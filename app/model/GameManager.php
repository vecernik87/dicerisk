<?php

namespace Model;

class GameManager {

	private $db;

	//private $data = false; // deprecated

	public function __construct(\Nette\Database\Context $database) {
		$this->db = $database;
	}

	public function createGame($user_id, $map_id, $size, $name) { // it would be nice to put this in gameclass...
		$game_row = $this->db->table('game')->insert(array('user_id' => $user_id, 'map_id' => $map_id, 'size' => $size, 'name' => $name,'status'=>  \Game::STATUS_PREPARE));
		return $this->getGame($game_row);
	}

	public function getGame($game) { // game can be ActiveRow OR int
		return new \Game($game, $this->db);
	}

	public function getGameList($user_id = false) {
		if ($user_id === false) {
			$list = $this->db->table('game')->fetchAll();
		} else {
			$list = $this->db->table('game')->where(':game_user.user_id', $user_id)->fetchAll();
		}
		foreach ($list as &$game) { // wrap all game rows as objects
			$game = $this->getGame($game); //this is at least 20% cooler than previous static-style functions!
		}
		return $list;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	public function getMapList4Select() { // this will be moved to another model.
		return $this->db->table('map')->fetchPairs('id', 'name');
	}

	public function getMap($map_id) { // this will be moved too...
		$map = $this->db->table('map')->wherePrimary($map_id)->fetch();
		//dump($map);
		$return = array();
		$maxX = 0;
		$maxY = 0;
		foreach ($map->related('region') as $region) {
			$tmp_region = array();
			$tmp_region['id'] = $region->id;
			$tmp_region['name'] = $region->name;
			$tmp_region['owner'] = rand(1, 5);
			foreach ($region->related('coord') as $coord) {
				$tmp_coord = array();
				$tmp_coord['x'] = $coord->x;
				if ($maxX < $coord->x)
					$maxX = $coord->x;
				$tmp_coord['y'] = $coord->y;
				if ($maxY < $coord->y)
					$maxY = $coord->y;
				$tmp_region['coord'][] = $tmp_coord;
			}

			$return[] = $tmp_region;
		}
		$return['regions'] = $return;
		$return['size'] = array('x' => $maxX * 20 + 60, 'y' => $maxY * 20 + 60);
		return $return;
	}

}
