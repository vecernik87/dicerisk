<?php

/**
 * Basic game class to access game as object
 */
class GameNotFoundException extends Exception {
	
}

class Game {

	const STATUS_PREPARE = 0x1,
			  STATUS_PLAY = 0x2,
			  STATUS_END = 0x4;

	/**
	 * @var \Nette\Database\Context
	 */
	private $db;

	/**
	 * @var Nette\Database\Table\ActiveRow
	 */
	private $data;

	/**
	 * Create game object 
	 * @param integer|object $game int = game id, object = ActiveRow
	 * @param \Nette\Database\Context $database DI database connection
	 */
	public function __construct($game, \Nette\Database\Context $database) {
		$this->db = $database;
		//dump($game);
		if ($game instanceof Nette\Database\Table\ActiveRow && $game->getTable()->getName() == 'game') {
			$this->data = $game;
		} else {
			if (!($this->data = $this->db->table('game')->wherePrimary($game)->fetch())) {
				//Nette\Diagnostics\Debugger::log('invalid game constructor');
				throw new \GameNotFoundException();
			}
		}
	}

	/**
	 * Simple getter to private $data protection
	 * @return Nette\Database\Table\ActiveRow data for current game object
	 */
	public function getData() {
		return $this->data;
	}

	/**
	 * Join user into $this game
	 * @param int $user_id
	 * @return boolean TRUE for successfull join, FALSE when unable to join
	 */
	public function joinUser($user_id) { // possible rename to "join()" ?
		if (!$this->isAbleJoin($user_id)) {
			return false; // cant join
		} else {
			$this->db->table('game_user')->insert(array('user_id' => $user_id, 'game_id' => $this->data->id));
			return true; // joined
		}
	}

	public function leaveUser($user_id) {
		if ($this->isAbleLeave($user_id)) {
			return ($this->db->table('game_user')->where('game_id=? AND user_id=?', $this->data->id, $user_id)->delete() ? true : false);
// TODO - do not delete, instead set status "left"
		} else {
			return false;
		}
	}

	public function start($user_id) {
		if ($this->isAbleStart($user_id)) {
			$players = $this->getPlayers();
			$sequence = range(1, count($players));
			shuffle($sequence);
			foreach ($players as $player) {
				$player->update(array('sequence' => (array_shift($sequence)))); // set random sequence for every player in this game
			}
			$this->insertStep($user_id, 'init data');
			$step = $this->insertStep($this->getNextPlayer(), '');
			$this->data->update(array('status' => self::STATUS_PLAY, 'game_step_id' => $step->getPrimary()));

			return true;
		} else {
			return false;
		}
	}

	public function setStep($uid, $data) {
		if ($this->getCurrentPlayer()->id == $uid) {
			$this->updateStep($uid, $data);
			$step = $this->insertStep($this->getNextPlayer($uid), '');
			$this->data->update(array('game_step_id' => $step->getPrimary()));
			return true;
		} else {
			return false;
		}
	}

	private function updateStep($user_id, $data) {
		$this->data->ref('game_step')->update(array('user_id' => $user_id, 'data' => $data));
	}

	private function insertStep($user_id, $data) {
		return $this->data->related('game_step')->insert(array('user_id' => $user_id, 'data' => $data));
	}

	public function getNextPlayer($user_id = null) {
		$players = $this->getPlayers('user_id');
		if ($user_id !== null) {
			while (list($uid, $val) = each($players)) {
				if ($user_id == $uid) {

					if (key($players) !== null) {
						return key($players);
					} else {
						break;
					}
				}
			}
		}
		reset($players);
		return key($players);
	}

	public function getCurrentPlayer() {
		$user = $this->data->ref('game_step')->ref('user');
		return $user;
	}

	public function getPlayerCount() {
		return $this->data->related('game_user')->count('*');
	}

	public function getPlayers($key = null) {
		$query = $this->data->related('game_user')->select('game_user.*,user.name')->order('sequence, id');
		if ($key === null) {
			return $query->fetchAll();
		} else {
			return $query->fetchPairs($key);
		}
	}

	public function isAbleJoin($user_id) {
		return !$this->isFull() && !$this->isJoined($user_id);
	}

	public function isAbleLeave($user_id) {
		return $this->isJoined($user_id) && $this->data->status == self::STATUS_PREPARE;
	}

	public function isAbleStart($user_id) {
		return $this->isFull() && $this->data->status == self::STATUS_PREPARE && $this->data->user_id == $user_id;
	}
	public function isCurrentPlayer($user_id){
		if($this->data->status == self::STATUS_PLAY){
			return $this->getCurrentPlayer()->id == $user_id?true:false;
		}else{
			return false;
		}
	}

	public function isJoined($user_id) {
		if ($user_id === null) {
			return false;
		} else {
			return ($this->data->related('game_user')->where('user_id=?', $user_id)->fetch() ? true : false);
		}
	}

	public function isFull() {
		return (($this->getPlayerCount() < $this->data->size) ? false : true);
	}

}
